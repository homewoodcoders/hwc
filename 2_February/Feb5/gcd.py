import math

# make our own
def greatest_common_denominator():
    m = int(input("enter the first postive number: "))
    n = int(input("enter the second postive number: "))

    if m == 0 and n ==0:
        return("Invalid input")
    if m == 0:
        return(f"GCD is {n}")
    if n == 0:
        return(f"GCD is {m}")
    
    while m != n:
        if m > n:
            m = m-n
        if n > m:
            n = n-m
    return(f"GCD is {m}")



if __name__=='__main__':
    print(greatest_common_denominator())

# or use a already existing modules
math.gcd(24,18)

# more often than not that thing already exist