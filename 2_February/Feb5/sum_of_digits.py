
def sum_digits():
    number=int(input("input a number: "))
    result = 0
    remainder = 0

    while number != 0:
        remainder = number % 10
        result = result + remainder
        number = int(number / 10)

    print(f'The sum of all digits is {result}')


if __name__=='__main__':
    sum_digits()