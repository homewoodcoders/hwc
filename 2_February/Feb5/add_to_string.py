
number = 3
b = 'True'
string = 'oranges'

# let's use this instead now that we know tuple and using python 3
print('i have {0} {2} and this is {1}'.format(number, b, string))

print(f"is it {b} that this has {number} {string}")

# this is okay too
print('i have %d %s and this is %s' %(number, b, string))