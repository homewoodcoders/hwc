class Employee:

    raise_amount = 1.04
    number_of_employee = 0

    def __init__ (self,first,last,pay):
        self.first = first
        self.last = last 
        self.pay = pay
        # can be self.payroll = pay
        self.email = first + '.' + last + '@company.com'

        Employee.number_of_employee += 1

    def fullname(self):
        return (self.first + ' ' + self.last)
    def apply_raise(self):
        self.pay *= self.raise_amount

emp_1 = Employee('Felix','Turner',60000)
emp_2 = Employee('Robbie','Nichols',85000)


print(Employee.number_of_employee)