class car:
    
    number_of_cars = 0
    
    def __init__(self, color, price, brand, depreciation_amount):
        self.color = color
        self.price = price
        self.brand = brand
        self.depreciation_amount = depreciation_amount

        car.number_of_cars += 1

    def depreciation(self):
        self.price *= self.depreciation_amount
    
car_1 = car('blue',10000, 'BMW', 0.90)
car_2 = car('red', 2000, 'toyota', 0.87)
car_3 = car('green', 40000, 'tesla', 0.99)

print(car_1.price)
car_1.depreciation()
print(car_1.price)

print(car.number_of_cars)