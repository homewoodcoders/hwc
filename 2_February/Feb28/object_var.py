class Employee:

    raise_amount = 1.04

    def __init__ (self,first,last,pay):
        self.first = first
        self.last = last 
        self.pay = pay
        # can be self.payroll = pay
        self.email = first + '.' + last + '@company.com'

    def fullname(self):
        return (self.first + ' ' + self.last)
    def apply_raise(self):
        self.pay *= self.raise_amount

emp_1 = Employee('Felix','Turner',60000)
emp_2 = Employee('Robbie','Nichols',85000)


print(emp_1.pay)
emp_1.apply_raise()
print(emp_1.pay)
print(emp_1.__dict__)