class SchoolMember:
    '''Represents any school member.'''
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print('(Initialized SchoolMember: {})'.format(self.name))

    def tell(self):
        '''Tell my details.'''
        print('Name:{0} Age:{1}'.format(self.name, self.age))


class Teacher(SchoolMember):
    '''Represents a teacher.'''
    def __init__(self, name, age, salary):
        SchoolMember.__init__(self, name, age)
        self.salary = salary
        print('(Initialized Teacher: {})'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('Salary: {}'.format(self.salary))


class Student(SchoolMember):
    '''Represents a student.'''
    def __init__(self, name, age, grades, fees):
        SchoolMember.__init__(self, name, age)
        self.grades = grades
        self.fees = fees
        print('(Initialized Student: {})'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('grades: {0} fees: {1}'.format(self.grades, self.fees))

t = Teacher('Mr. Nichols', 51, 3000000000000)
s = Student('Jared', 5, 4.0, 12910)

# prints a blank line
print()

members = [t, s]

for member in members:
    # Works for both Teachers and Students
    member.tell()