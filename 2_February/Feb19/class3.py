

class BankAccount:
    def __init__(self, name):
        self.user_name = name
        self.balance = 0.0

        
    
    def show_balance(self):
        print("{0} has a balance of: ${1}".format(self.user_name, self.balance))
    
    def deposit_money(self,amount):
        self.balance += amount
        print("{0} has deposited: ${1}".format(self.user_name, amount))
    
    def withdraw_money(self, amount):
        if amount > self.balance:
            print("insufficient funds!")
        else:
            self.balance -= amount
            print("{0} has withdrew: ${1}".format(self.user_name, amount))


savings_account = BankAccount("Jared")
savings_account.deposit_money(1000)
savings_account.show_balance()
savings_account.withdraw_money(420)
savings_account.show_balance()

savings_account = BankAccount("Ziyang")
savings_account.deposit_money(50)
savings_account.show_balance()
savings_account.withdraw_money(40000)
savings_account.show_balance()
