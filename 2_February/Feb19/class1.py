import math

class ArcLength:
    def __init__(self):
        self.radius = 0
        self.angle  = 0


    def calculate_arc_length(self):
        return( 2 * math.pi * self.radius * self.angle/360)
        

#make object a0042

a0042 = ArcLength()


# change the data attribute of a0042
a0042.radius = 9
a0042.angle  = 75

print("radius is {0} and angle is {1}".format(a0042.radius, a0042.angle))

print(a0042.calculate_arc_length())