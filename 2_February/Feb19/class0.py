class ClassName:

    def __init__(self):
        self.abcd = 4


    def methods0(self):
        # add two
        self.abcd += 2

    def methods1(self):
        # subtract one
        self.abcd -= 1
    
    def methods2(self):
        self.abcd *= 2
    
    def methods3(self):
        self.abcd /= 2


