

class Birds:
    def __init__(self, bird_name):
        self.bird_name = bird_name
    
    def flying_bird(self):
        print("{0} flies about the clouds".format(self.bird_name))

    def not_flying(self):
        print("{0} can't fly lol".format(self.bird_name))



vulture = Birds("Vulture")
vulture.flying_bird()

crane = Birds("Crane")
crane.flying_bird()

emu = Birds("Emu")
emu.not_flying()

penguin = Birds("Penguin")
penguin.not_flying()