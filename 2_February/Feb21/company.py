class Employee:
    def __init__ (self,first,last,pay):
        self.first = first
        self.last = last 
        self.pay = pay
        # can be self.payroll = pay
        self.email = first + '.' + last + '@company.com'
    def fullname(self):
        return (self.first + ' ' + self.last)    
emp_1 = Employee('Felix','Turner',60000)
emp_2 = Employee('Robbie','Nichols',85000)

print(emp_1.email)
print(emp_2.fullname())
print(Employee.fullname(emp_2))

#create a class that can give me a red Toyota car with a price tag of 4500 and a 
# blue BMW car with a price tag of 15000
#return the red car's price
#return the blue car's brand