import math

print(41+55) #addition
print(24-10) #subtraction
print(3*5)   #multiplication
print(7/2)   #division
print(2**5)  #exponents
print(13%5)  #remainder

print(math.fabs(-23))  #absolute value
print(math.gcd(24,18)) #return the greatest common divisor
print(math.exp(2))     #raise e to the power
print(math.log(5))     #logarithm
print(math.sqrt(49))   #square root
print(math.sin(2))     #trigonometric
print(math.pi)         #pi 