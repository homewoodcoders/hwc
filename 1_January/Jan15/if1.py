
if True:
    print("foo")

# remember to use  == two equals sign because = is used to declare variables
if 2 == 2:
    print("bar")

else:
    print("car")
    

# this does the exact same thing as


if True:
    print("f")

# elif stands for else if, it just chains nested if else statements together.

elif 2 > 1:

    print("hello there")

else:
    print("....")