
x = 45
y = 3.2
this_is_a_string = "haha"
this_is_another_string = 'you can use single or double quotation mark to make a string'

#you can hover the letters to see their data type.

print(x)

print(this_is_a_string)

print(y)

print(this_is_another_string)

print(this_is_a_string)

