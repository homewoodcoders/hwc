import math

print("quadratic formula")
a = float(input("a = "))
b = float(input("b = "))
c = float(input("c = "))


#discriminant
#this will return an error if there are imaginary roots
d = math.sqrt(b**2 - 4 * a * c)


#solve
root_one = (-b+ d) / (2 * a)
root_two = (-b- d) / (2 * a)


#check the number of real roots
if d == 0:
    print("there are only one real root: ", root_one)


#because we have already tested if the discriminant is negative or zero,
#the only option left is positive.
else:


    # the %f tells the program that a float number is suppose to be here
    # the variables at the end will substitute in for the %f
    print("there are two real root: %f and %f" % (root_one, root_two))



# part two

axisOfSym = (-b)/(2*a)
print("axis of symmetry: %f" % axisOfSym)

y_value = a*(axisOfSym)**2 + b*(axisOfSym) + c 
print("the vertex is ( %f , %f )" % (axisOfSym, y_value))
