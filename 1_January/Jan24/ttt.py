# board

"""
X = 1
O = 10
"""
turn = 1

xw = "x win"
ow = "o win"

row0 = [0,0,0] # [0][0] | [0][1] | [0][2]
row1 = [0,0,0] # [1][0] | [1][1] | [1][2]
row2 = [0,0,0] # [2][0] | [2][1] | [2][2]

board = [row0, row1, row2]

print("0 is empty space, 1 is X, 10 is O")

# loop
while 1:
    # game
    if turn%2 == 1:
        print("X turn")
        row_number = int(input("which row? [0][1][2]: "))
        column_number = int(input("which column? [0][1][2]: "))
        if board[row_number][column_number] == 1 or board[row_number][column_number] == 10:
            print("place already taken")
            continue
        else:
            board[row_number][column_number] = 1
        print('you placed an X at [%d][%d]' % (row_number, column_number))
        print(row0)
        print(row1)
        print(row2)
    else:
        print("O turn")
        row_number = int(input("which row? [0][1][2]: "))
        column_number = int(input("which column? [0][1][2]: "))
        if board[row_number][column_number] == 1 or board[row_number][column_number] == 10:
            print("place already taken")
            continue
        else:
            board[row_number][column_number] = 10
        print('you placed an O at [%d][%d]' % (row_number, column_number))
        print(row0)
        print(row1)
        print(row2)

    # row win
    if (sum(row0) == 3) or (sum(row1) == 3) or (sum(row2) == 3):
        print(xw)
        break
    elif (sum(row0) == 30) or (sum(row1) == 30) or (sum(row2) == 30):
        print(ow)
        break

    # column win
    elif (board[0][0] + board[1][0] + board[2][0] == 3) or (board[0][1] + board[1][1] + board[2][1] == 3) or (board[0][2] + board[1][2] + board[2][2] == 3) :
        print(xw)
        break
    elif (board[0][0] + board[1][0] + board[2][0] == 30) or (board[0][1] + board[1][1] + board[2][1] == 30) or (board[0][2] + board[1][2] + board[2][2] == 30) :
        print(ow)
        break



    elif (board[0][0] + board[1][1] + board[2][2] == 3) or (board[0][2] + board[1][1] + board[2][0] == 3):
        print(xw)
        break
    elif (board[0][0] + board[1][1] + board[2][2] == 30) or (board[0][2] + board[1][1] + board[2][0] == 30):
        print(ow)
        break


    elif not(0 in row0 or 0 in row1 or 0 in row2):
        print("tie")
        break
    
    else:
        print('...')
        turn += 1