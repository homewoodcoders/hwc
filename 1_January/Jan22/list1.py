

this_list = ['bananas', 'apples', 'oranges']

# print the 0th element in the list, which is 'bananas'.
print(this_list[0])

# print the 2nd element in the list.
print(this_list[2])

# print the first last element
print(this_list[-1])