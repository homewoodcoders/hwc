
# a string is a tuple

string = 'bbcdelfgho'

print(string[8] + string[4] + string[5]*2 + string[9])

#  h is the eighth character of the string, e is the fourth character, etc.


'''
you cannot do this:
    string[8] = 't'

because tuple cannot be changed once it is declared

'''
