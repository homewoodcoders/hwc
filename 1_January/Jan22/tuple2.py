print("swap numbers\n")

x = int(input("the value of the first number: "))
y = int(input("the value of the second number: "))

print(x, y)

# the (x, y) is a tuple
print('%d and %d swapping...' % (x, y))
print(x, 'and', y, 'swapping...')

#using tuple
(x, y) = (y, x)

print(x, y)

#not using tuple
z = x 
x = y
y = z 

print(x, y)