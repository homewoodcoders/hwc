

tuple0 = (2, 3)

# you can just print the tuple like a list
print(tuple0)

# or you can assign the tuple to multiple variables 
# just like how you can assign one number to one variable

(a, b) = tuple0

# a = tuple0[0]
# b = tuple0[1]

print(a, b)
print('The 0 th element is: ', a)
print('The first element is: ', b)
