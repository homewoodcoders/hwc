
# you can have numbers in a list.
list0 = [1, 3, 4, 4, 12, 7]

# you can have numbers, string, Boolean, and float all in the same list.
list1 = [0, "h", True, 6.2]

# you can even have list inside a list!
list2 = [list1, list0]


print(list0)
print(list1)
print(list2)