
list_0 = [0, 1, 5, 6, 3, 4, 4, 4, 0, 10, 99]

list_0.sort()

# add 100 to the end of the list
list_0.append(100)

# print how many 4 are in the array
print( list_0.count(4) )

# insert ah at index 3
list_0.insert(3, 'ah')

# pop the guy at index 9
list_0.pop(9)

# remove the first 4 it sees
list_0.remove(4)

print(list_0)