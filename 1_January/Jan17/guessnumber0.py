import random
#First declare the random interger for guessing
secret_Number = random.randint(0,20)

# start the guessing loop
while True:
    
    # the input() ask the player a question and the int() convert the input into integers
    guess = int(input("Guess a number 0 - 100: "))
    
    # If the player guess the number right it will say Nice! and break/exit the loop
    if guess == secret_Number:
        print("Nice!")
        #break terminates the execution of a for or while loop
        break

    # if the guess is higher than the secret_Number then it tells the player it's larger than the secret_Number
    elif guess > secret_Number:
        print("Too large")
    
    # vise versa
    # This time we do not need (guess < secret_Number) because the guess can now only be smaller than the secret_Number
    # we have eliminated all other possible options. 
    else:
        print("Too small")
