
# import pygame and init the pygame modules
import pygame
pygame.init()

# define the colors
PURPLE = ( 36, 21, 226)

# define the size of the screen
size = (width, height) = 700, 700
# make the screen 
screen = pygame.display.set_mode(size)
# make the clock
clock = pygame.time.Clock()
# define the fps
fps = 144

# make the class of dvd.
class DVD:
    # init the speed and the image
    def __init__(self, a, b, image):
        self.speed = [a, b]
        self.dvd_image = pygame.image.load(image)
    # load the hitbox
    def hitbox(self):
        self.dvd_rect = self.dvd_image.get_rect()
    # function move
    def move(self):

        self.dvd_rect = self.dvd_rect.move(self.speed)

    def startpos(self, x, y):
        self.dvd_rect = self.dvd_rect.move([x, y])

    # function blit
    def banana(self):
        screen.blit(self.dvd_image, self.dvd_rect)


# make objects
dvd1 = DVD(2,1, "dvd.gif")
dvd2 = DVD(1,1, "dvd.gif")
dvd3 = DVD(2,3, "dvd.gif")

# put objects in an array
dvds = [dvd1, dvd2, dvd3]

# for loop the objects
for dvd in dvds:
    # load the hitbox
    dvd.hitbox()
# setup starting pos
dvd1.startpos(  0,   0)
dvd2.startpos(300, 300)
dvd3.startpos(200, 300)

# set up the while loop
done = False
while not done:

    # set up a way to quit the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    # for loop the objects
    for dvd in dvds:

        dvd.move()
        # call the function move (move each object)

        # build the wall
        if dvd.dvd_rect.left < 0:
            dvd.speed[0] = -dvd.speed[0]

        if dvd.dvd_rect.top < 0:
            dvd.speed[1] = -dvd.speed[1]

        if dvd.dvd_rect.right > width:
            dvd.speed[0] = -dvd.speed[0]

        if dvd.dvd_rect. bottom > height:
            dvd.speed[1] = -dvd.speed[1]

    # fill the screen with a color
    screen.fill(PURPLE)

    # blit every object
    for dvd in dvds:
        dvd.banana()

    # update the screen
    pygame.display.flip()

    # set up the fps
    clock.tick(fps)

# quit the game
pygame.quit()
