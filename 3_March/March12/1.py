# Import a library of functions called 'pygame'
import pygame
# Initialize the game engine
pygame.init()

# Define some colors
BLACK    = (0, 0, 0)
WHITE    = (255, 255, 255)

# make the window
size = (700, 500)

screen = pygame.display.set_mode(size)

