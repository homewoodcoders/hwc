
# import pygame and init (start) pygame modules (functions)
import pygame
pygame.init()

# set up the screen (graphical interface)
size = (width, height) = (700, 700)
screen = pygame.display.set_mode(size)

# set up the colors
PURPLE = (139, 2, 232)
``
# set up the fps
clock = pygame.time.Clock()

# set up the speed vector
speed = [2,1]

# set up the image
dvd = pygame.image.load("dvd.gif")

# set up the rectangle
dvd_rect = dvd.get_rect()

# set up the game loop and a way to quit
done = False

while not done:

    # set up a way to quit
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # got to go fast
    dvd_rect = dvd_rect.move(speed)

    # set up the walls and set up bounce
    if dvd_rect.left <= 0:
        speed[0] = -speed[0]

    if dvd_rect.right >= width-1:
        speed[0] = -speed[0]

    if dvd_rect.top <= 0:
        speed[1] = -speed[1]

    if dvd_rect.bottom >= height-1:
        speed[1] = -speed[1]

    # refill the screen
    screen.fill(PURPLE)

    # redraw the image and the rectangle
    screen.blit(dvd, dvd_rect)

    # update the display
    pygame.display.flip()

    # set up the fps
    clock.tick(120)

# quit
pygame.quit()