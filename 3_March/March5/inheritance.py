class Vehicles:
    def __init__(self, color, price):
        self.color = color
        self.price = price


class Bicycle(Vehicles):
    def __init__(self, color, price, bicycle_type):
        Vehicles.__init__(self, color, price)
        self.bicycle_type = bicycle_type

    def show(self):
        print(self.color + ' Bicycle')
        print('${}'.format(self.price))
        print(self.bicycle_type)
        print('---------')


class Car(Vehicles):
    def __init__(self, color, price, year):
        Vehicles.__init__(self, color, price)
        self.year = year

    def show(self):
        print(self.color + ' Car')
        print('${}'.format(self.price))
        print(self.year)
        print('---------')

car_1 = Car('blue', 1000, 1999)
car_2 = Car('silver', 3000, 2005)

bicycle_1 = Bicycle('red', 500, 'Folding Bicycle')
bicycle_2 = Bicycle('green', 800, 'BMX Bicycle')

car_1.show()
car_2.show()

bicycle_1.show()
bicycle_2.show()