class SchoolMember:
    def __init__(self, name, species, physics):
        self.name = name
        self.species = species
        self.physics = physics


class Teacher(SchoolMember):
    def __init__(self, name, species, physics, pay):
        super().__init__(self, name, species, physics)
        self.pay = pay

class Student(SchoolMember):
    def __init__(self, name, species, physics, debt):
        super().__init__(self, name, species, physics)
        self.debt = debt
